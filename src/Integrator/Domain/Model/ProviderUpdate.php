<?php

declare(strict_types=1);

namespace App\Integrator\Domain\Model;

use DateTime;

class ProviderUpdate
{
    private string $token;
    private string $status;
    private string $timestamp;
    private string $data;

    public function __construct(string $token, string $status, string $timestamp, array $data)
    {
        $this->status = $status;
        $this->timestamp = $timestamp;
        $this->data = json_encode($data, 512, JSON_THROW_ON_ERROR);
        $this->token = $token;
    }

    public function getStatus(): string
    {
        return $this->status;
    }

    public function setStatus(string $status): void
    {
        $this->status = $status;
    }

    public function getTimestamp(): string
    {
        return $this->timestamp;
    }

    public function setTimestamp(string $timestamp): void
    {
        $this->timestamp = $timestamp;
    }

    public function getData(): array
    {
        return json_decode($this->data, true);
    }

    public function setData(array $data): void
    {
        $this->data = json_encode($data, 512, JSON_THROW_ON_ERROR);
    }

    public function getToken(): string
    {
        return $this->token;
    }

    public function setToken(string $token): void
    {
        $this->token = $token;
    }

    public function isOlderThan(?ProviderUpdate $another): bool
    {
        if (null === $another) {
            return false;
        }

        $thisTimestamp = $this->convertToTimestamp($this->getTimestamp());
        $anotherTimestamp = $this->convertToTimestamp($another->getTimestamp());

        return $thisTimestamp < $anotherTimestamp;
    }

    private function convertToTimestamp(string $dateTime): int
    {
        return DateTime::createFromFormat(DateTime::ISO8601, $dateTime)->getTimestamp();
    }
}
