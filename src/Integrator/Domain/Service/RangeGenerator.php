<?php

declare(strict_types=1);

namespace App\Integrator\Domain\Service;

use App\Integrator\Domain\ValueObject\ValueRange;

class RangeGenerator
{
    private const DEFAULT_SHEET_NAME = 'Arkusz1';
    private const DEFAULT_FIRST_RANGE = 'A';
    private const DEFAULT_LAST_RANGE = 'Z';

    public function generate(ValueRange $valueRange): string
    {
        $sheet = $this->getSheetName();
        $start = $this->getStartRange($valueRange);
        $end = $this->getEndRange($valueRange);

        return "{$sheet}!{$start}:{$end}";
    }

    private function getSheetName(): string
    {
        return $_ENV['GOOGLE_SHEET_NAME'] ?? self::DEFAULT_SHEET_NAME;
    }

    public function getStartRange(ValueRange $valueRange): string
    {
        $firstKey = array_key_first($valueRange->getFirstValues());
        return $this->countRange($firstKey, self::DEFAULT_FIRST_RANGE);
    }

    public function getEndRange(ValueRange $valueRange): string
    {
        $lastKey = array_key_last($valueRange->getFirstValues());
        return $this->countRange($lastKey, self::DEFAULT_LAST_RANGE);
    }

    private function countRange($key, string $defaultRange): string
    {
        if (false === is_int($key)) {
            return $defaultRange;
        }

        return chr(64 + ++$key);
    }
}
