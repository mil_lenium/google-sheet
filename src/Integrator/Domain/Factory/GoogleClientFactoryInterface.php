<?php

declare(strict_types=1);

namespace App\Integrator\Domain\Factory;

use App\Integrator\Infrastructure\Service\GoogleClient;

interface GoogleClientFactoryInterface
{
    public function create(string $configPath): GoogleClient;
}
