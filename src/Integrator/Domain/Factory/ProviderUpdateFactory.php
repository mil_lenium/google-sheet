<?php

declare(strict_types=1);

namespace App\Integrator\Domain\Factory;

use App\Integrator\Domain\Model\ProviderUpdate;

class ProviderUpdateFactory
{
    public function create(array $data): ProviderUpdate
    {
        $token = $data['token'];
        $status = $data['status'];
        $timestamp = $data['timestamp'];
        $data = $data['data'];

        return new ProviderUpdate($token, $status, $timestamp, $data);
    }
}
