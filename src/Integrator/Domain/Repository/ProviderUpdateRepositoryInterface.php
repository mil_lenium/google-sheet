<?php

declare(strict_types=1);

namespace App\Integrator\Domain\Repository;

use App\Integrator\Domain\Model\ProviderUpdate;

interface ProviderUpdateRepositoryInterface
{
    public function save(ProviderUpdate $providerUpdate): void;

    public function findOneByToken(string $token): ?ProviderUpdate;

    public function findLatestOne(): ?ProviderUpdate;
}
