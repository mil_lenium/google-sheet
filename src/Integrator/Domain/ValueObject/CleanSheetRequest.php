<?php

declare(strict_types=1);

namespace App\Integrator\Domain\ValueObject;

final class CleanSheetRequest
{
    private string $fields;

    public static function create(string $fields): self
    {
        return new self($fields);
    }

    private function __construct(string $fields)
    {
        $this->fields = $fields;
    }

    public function toArray(): array
    {
        return [
            'updateCells' => [
                'range' => [
                    'sheetId' => 0,
                ],
                'fields' => $this->fields,
            ],
        ];
    }
}
