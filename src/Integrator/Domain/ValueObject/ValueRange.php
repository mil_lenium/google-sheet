<?php

declare(strict_types=1);

namespace App\Integrator\Domain\ValueObject;

final class ValueRange
{
    private array $values;

    public static function fromArray(array $values): self
    {
        return new self($values);
    }

    private function __construct(array $values)
    {
        $this->values = $values;
    }

    public function toArray(): array
    {
        return [
            'values' => $this->values,
        ];
    }

    public function getFirstValues(): array
    {
        return reset($this->values) ?: [];
    }
}
