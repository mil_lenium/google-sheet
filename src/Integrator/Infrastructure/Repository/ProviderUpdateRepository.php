<?php

declare(strict_types=1);

namespace App\Integrator\Infrastructure\Repository;

use App\Integrator\Domain\Model\ProviderUpdate;
use App\Integrator\Domain\Repository\ProviderUpdateRepositoryInterface;
use Doctrine\ODM\MongoDB\Repository\DocumentRepository;

class ProviderUpdateRepository extends DocumentRepository implements ProviderUpdateRepositoryInterface
{
    public function save(ProviderUpdate $providerUpdate): void
    {
        $this->getDocumentManager()->persist($providerUpdate);
        $this->getDocumentManager()->flush();
    }

    public function findOneByToken(string $token): ?ProviderUpdate
    {
        return $this->findOneBy([
            'token' => $token,
        ]);
    }

    public function findLatestOne(): ?ProviderUpdate
    {
        $qb = $this->getDocumentManager()->createQueryBuilder(ProviderUpdate::class);
        $query = $qb
            ->sort('timestamp', 'DESC')
            ->limit(1)
            ->getQuery();

        foreach ($query->execute() as $providerUpdate) {
            return $providerUpdate;
        }

        return null;
    }
}
