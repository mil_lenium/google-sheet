<?php

declare(strict_types=1);

namespace App\Integrator\Infrastructure\Factory;

use App\Integrator\Domain\Factory\GoogleClientFactoryInterface;
use App\Integrator\Infrastructure\Service\GoogleClient;
use Google_Client;

class GoogleClientFactory implements GoogleClientFactoryInterface
{
    public function create(string $configPath): GoogleClient
    {
        $client = new GoogleClient(new Google_Client());
        $client->setAuthConfig($configPath);

        return $client;
    }
}
