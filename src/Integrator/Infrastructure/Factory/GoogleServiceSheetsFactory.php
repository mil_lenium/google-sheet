<?php

declare(strict_types=1);

namespace App\Integrator\Infrastructure\Factory;

use App\Integrator\Infrastructure\Service\GoogleClient;
use App\Integrator\Infrastructure\Service\GoogleServiceSheets;
use Google_Service_Sheets;

class GoogleServiceSheetsFactory
{
    private GoogleClient $client;

    public function __construct(GoogleClient $client)
    {
        $this->client = $client;
    }

    public function create(): GoogleServiceSheets
    {
        $this->client->getClient()->setScopes(Google_Service_Sheets::SPREADSHEETS);
        $serviceSheets = new Google_Service_Sheets($this->client->getClient());
        return new GoogleServiceSheets($serviceSheets);
    }
}
