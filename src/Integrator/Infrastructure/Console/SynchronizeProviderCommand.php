<?php

declare(strict_types=1);

namespace App\Integrator\Infrastructure\Console;

use App\Integrator\Domain\Factory\ProviderUpdateFactory;
use App\Integrator\Domain\Model\ProviderUpdate;
use App\Integrator\Domain\Repository\ProviderUpdateRepositoryInterface;
use App\Integrator\Domain\ValueObject\ValueRange;
use App\Integrator\Infrastructure\Service\GoogleSheetCleanerInterface;
use App\Integrator\Infrastructure\Service\GoogleSheetUpdaterInterface;
use App\Integrator\Infrastructure\Service\ProviderFileReader;
use App\Integrator\Infrastructure\Service\SheetIdProviderInterface;
use App\Integrator\Infrastructure\Service\UpdateDecider;
use Doctrine\ODM\MongoDB\DocumentManager;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class SynchronizeProviderCommand extends Command
{
    protected static $defaultName = 'integrator:provider:synchronize';

    private GoogleSheetCleanerInterface $googleSheetCleaner;
    private GoogleSheetUpdaterInterface $googleSheetUpdater;
    private SheetIdProviderInterface $sheetIdProvider;
    private ProviderUpdateRepositoryInterface $repository;
    private ProviderFileReader $dataReader;
    private ProviderUpdateFactory $updateFactory;
    private UpdateDecider $updateDecider;

    public function __construct(
        GoogleSheetCleanerInterface $googleSheetCleaner,
        GoogleSheetUpdaterInterface $googleSheetUpdater,
        SheetIdProviderInterface $sheetIdProvider,
        ProviderFileReader $dataReader,
        ProviderUpdateFactory $updateFactory,
        UpdateDecider $updateDecider,
        DocumentManager $documentManager
    ) {
        parent::__construct();
        $this->googleSheetCleaner = $googleSheetCleaner;
        $this->googleSheetUpdater = $googleSheetUpdater;
        $this->sheetIdProvider = $sheetIdProvider;
        $this->dataReader = $dataReader;
        $this->updateFactory = $updateFactory;
        $this->updateDecider = $updateDecider;

        /** @var ProviderUpdateRepositoryInterface $repository */
        $repository = $documentManager->getRepository(ProviderUpdate::class);
        $this->repository = $repository;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $sheetId = $this->sheetIdProvider->provide();
        $data = $this->dataReader->read();

        $providerUpdate = $this->updateFactory->create($data);

        if (false === $this->updateDecider->needUpdate($providerUpdate)) {
            return 0;
        }

        $this->repository->save($providerUpdate);
        $this->googleSheetCleaner->clean($sheetId);
        $this->googleSheetUpdater->update($sheetId, ValueRange::fromArray($data['data']));

        $output->writeln("Sheet id: {$sheetId}");
        return 1;
    }
}
