<?php

declare(strict_types=1);

namespace App\Integrator\Infrastructure\Service;

use Exception;
use Symfony\Component\Filesystem\Filesystem;

class ProviderFileReader
{
    private string $path;
    private Filesystem $filesystem;

    public function __construct(string $path, Filesystem $filesystem)
    {
        $this->path = $path;
        $this->filesystem = $filesystem;
    }

    public function read(): array
    {
        try {
            $file = file_get_contents($this->path);
        } catch (Exception $exception) {
            throw new Exception("File [{$this->path}] not found.");
        }

        return json_decode($file, true, JSON_THROW_ON_ERROR);
    }
}
