<?php

declare(strict_types=1);

namespace App\Integrator\Infrastructure\Service;

interface GoogleSheetCleanerInterface
{
    public function clean(string $sheetId): void;
}
