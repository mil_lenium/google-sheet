<?php

declare(strict_types=1);

namespace App\Integrator\Infrastructure\Service;

use Google_Service_Sheets;
use Google_Service_Sheets_BatchUpdateSpreadsheetRequest as BatchUpdateRequest;
use Google_Service_Sheets_BatchUpdateSpreadsheetResponse as BatchUpdateResponse;
use Google_Service_Sheets_ValueRange as ValueRange;

class GoogleServiceSheets
{
    private Google_Service_Sheets $serviceSheets;

    public function __construct(Google_Service_Sheets $serviceSheets)
    {
        $this->serviceSheets = $serviceSheets;
    }

    public function getServiceSheets(): Google_Service_Sheets
    {
        return $this->serviceSheets;
    }

    public function update(string $sheetId, string $range, ValueRange $valueRange, array $configuration): void
    {
        $this->getServiceSheets()->spreadsheets_values->update($sheetId, $range, $valueRange, $configuration);
    }

    public function batchUpdate(string $sheetId, BatchUpdateRequest $request): BatchUpdateResponse
    {
        return $this->getServiceSheets()->spreadsheets->batchUpdate($sheetId, $request);
    }
}
