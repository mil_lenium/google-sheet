<?php

declare(strict_types=1);

namespace App\Integrator\Infrastructure\Service;

interface SheetIdProviderInterface
{
    public function provide(): string;
}
