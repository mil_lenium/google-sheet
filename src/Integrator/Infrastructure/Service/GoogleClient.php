<?php

declare(strict_types=1);

namespace App\Integrator\Infrastructure\Service;

use Google_Client;

class GoogleClient
{
    private Google_Client $client;

    public function __construct(Google_Client $client)
    {
        $this->client = $client;
    }

    public function getClient(): Google_Client
    {
        return $this->client;
    }

    public function setAuthConfig(string $config): void
    {
        $this->client->setAuthConfig($config);
    }

    public function setScopes(string $scopes): void
    {
        $this->client->setScopes($scopes);
    }
}
