<?php

declare(strict_types=1);

namespace App\Integrator\Infrastructure\Service;

use App\Integrator\Domain\ValueObject\ValueRange;

interface GoogleSheetUpdaterInterface
{
    public function update(string $sheetId, ValueRange $valueRange): void;
}
