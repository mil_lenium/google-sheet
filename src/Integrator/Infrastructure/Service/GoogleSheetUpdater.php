<?php

declare(strict_types=1);

namespace App\Integrator\Infrastructure\Service;

use App\Integrator\Domain\Service\RangeGenerator;
use App\Integrator\Domain\ValueObject\ValueRange;
use Google_Service_Sheets_ValueRange as SheetValueRange;

class GoogleSheetUpdater implements GoogleSheetUpdaterInterface
{
    private const DEFAULT_INPUT = 'RAW';
    private GoogleServiceSheets $serviceSheets;
    private RangeGenerator $rangeGenerator;

    public function __construct(GoogleServiceSheets $serviceSheets, RangeGenerator $rangeGenerator)
    {
        $this->serviceSheets = $serviceSheets;
        $this->rangeGenerator = $rangeGenerator;
    }

    public function update(string $sheetId, ValueRange $valueRange): void
    {
        $configuration = ['valueInputOption' => self::DEFAULT_INPUT];
        $range = $this->rangeGenerator->generate($valueRange);
        $valueRange = new SheetValueRange($valueRange->toArray());

        $this->serviceSheets->update($sheetId, $range, $valueRange, $configuration);
    }
}
