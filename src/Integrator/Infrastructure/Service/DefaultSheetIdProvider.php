<?php

declare(strict_types=1);

namespace App\Integrator\Infrastructure\Service;

class DefaultSheetIdProvider implements SheetIdProviderInterface
{
    private const SHEET_ID_ENV = 'GOOGLE_SHEET_DEFAULT_ID';

    public function provide(): string
    {
        return $_ENV[self::SHEET_ID_ENV];
    }
}
