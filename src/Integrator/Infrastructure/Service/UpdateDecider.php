<?php

declare(strict_types=1);

namespace App\Integrator\Infrastructure\Service;

use App\Integrator\Domain\Model\ProviderUpdate;
use App\Integrator\Domain\Repository\ProviderUpdateRepositoryInterface;
use Doctrine\ODM\MongoDB\DocumentManager;

class UpdateDecider
{
    private ProviderUpdateRepositoryInterface $repository;

    public function __construct(DocumentManager $documentManager)
    {
        /** @var ProviderUpdateRepositoryInterface $repository */
        $repository = $documentManager->getRepository(ProviderUpdate::class);
        $this->repository = $repository;
    }

    public function needUpdate(ProviderUpdate $providerUpdate): bool
    {
        $existentUpdate = $this->repository->findOneByToken($providerUpdate->getToken());
        $latestUpdate = $this->repository->findLatestOne();

        if ($providerUpdate->isOlderThan($latestUpdate)) {
            return false;
        }

        if (null !== $existentUpdate) {
            return false;
        }

        return true;
    }
}
