<?php

declare(strict_types=1);

namespace App\Integrator\Infrastructure\Service;

use App\Integrator\Domain\ValueObject\CleanSheetRequest;
use Google_Service_Sheets_BatchUpdateSpreadsheetRequest as BatchUpdateSpreadsheetRequest;
use Google_Service_Sheets_UpdateCellsRequest as UpdateCellsRequest;

class GoogleSheetCleaner implements GoogleSheetCleanerInterface
{
    private GoogleServiceSheets $serviceSheets;

    public function __construct(GoogleServiceSheets $serviceSheets)
    {
        $this->serviceSheets = $serviceSheets;
    }

    public function clean(string $sheetId): void
    {
        $request = CleanSheetRequest::create('*');
        $requests = new UpdateCellsRequest($request->toArray());

        $body = new BatchUpdateSpreadsheetRequest();
        $body->setRequests($requests);

        $this->serviceSheets->batchUpdate($sheetId, $body);
    }
}
