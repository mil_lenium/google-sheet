# google-sheet

Description:
 An application for importing data from json file to google sheet, synchronizing their states.
 
Technologies:
 - PHP 7.4.0
 - Symfony 5.0.0
 - MongoDB 4.2.3

How to run:
 - copy .env file `cp .env.local .env`
 - `docker-compose up -d`
 
Command:
 - `php bin/console integrator:provider:synchronize`
